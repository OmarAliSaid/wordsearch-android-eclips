package net.codecanyon.trimax.android.wordsearch.game;

import net.codecanyon.trimax.android.wordsearch.R;
import net.codecanyon.trimax.android.wordsearch.prefs.PrefManager;
import net.codecanyon.trimax.android.wordsearch.prefs.Settings;
import net.codecanyon.trimax.android.wordsearch.prefs.WSDatabase;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.GridView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class GameActivity extends Activity  implements WSLayout.OnWordHighlightedListener {

	//private InterstitialAd mInterstitialAd;

	private int rows ;
	private int cols ;
	private ProgressDialog dialog;
	private String[] wordList;
	private RelativeLayout word_list_label_group;
	private WordTableAdapter wordListAdapter;
	private WSLayout grid;
	private char[][] board;
	private Set<Word> solution = new HashSet<Word>();
	private final Direction[] directions = Direction.values();
	private boolean[][] lock;
	private int[] randIndices;
	private Set<Word> foundWords = new HashSet<Word>();
	private GridView grd_word_list;
	private String puzzel_word;
	
	Category category;
	PrefManager prefManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	/*	ActionBar actionBar = getActionBar();

		if(actionBar != null){
			actionBar.hide();
		}*/

		setContentView(R.layout.activity_game);

		word_list_label_group = (RelativeLayout)findViewById(R.id.word_list_label_group);
		grd_word_list = (GridView)findViewById(R.id.grd_word_list);
		
			
		grid = (WSLayout)findViewById(R.id.game_board);
		prefManager = new PrefManager(this);
		
	/*	mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_pid));

		mInterstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {

				startNewGame();

			}

		});*/

		/*requestNewInterstitial();*/
		
		rows = grid.getNumRows();
		cols = grid.getNumColumns();
		board = new char[rows][cols];
		lock = new boolean[rows][cols];

		if(savedInstanceState != null && savedInstanceState.get("words") == null)
			savedInstanceState = null;


		if (savedInstanceState == null) {

			showDialog();
			Runnable r = new Runnable() {

				@Override
				public void run() {


					selectWords();

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							clearBoard();
							randomizeWords();

							grid.setOnWordHighlightedListener(GameActivity.this);
							prepareBoard();
							if (dialog != null)
								dialog.dismiss();
							if (word_list_label_group != null)
								word_list_label_group.setVisibility(View.VISIBLE);
							grid.setVisibility(View.VISIBLE);
							AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
							anim.setDuration(500);
							grid.startAnimation(anim);

						}
					});
				}
			};


			new Thread(r).start();

		} else {

			wordList = savedInstanceState.getStringArray("words");
			category = savedInstanceState.getParcelable("category");
			clearBoard();
			List<Word> words = savedInstanceState.getParcelableArrayList("solution");
			for (Word word : words) {
				embedWord(word);
			}
			words = savedInstanceState.getParcelableArrayList("found");
			foundWords = new HashSet<Word>(words);
			grd_word_list.setLayoutAnimation(null);

			grid.setOnWordHighlightedListener(this);
			prepareBoard();
			grid.setVisibility(View.VISIBLE);
			if(word_list_label_group!=null)
				word_list_label_group.setVisibility(View.VISIBLE);

			if (dialog != null)
				dialog.dismiss();
		}

	}

	@Override
	protected void onStart() {		
		super.onStart();
		
		boolean keepAwake = Settings.getBooleanValue(this, getResources().getString(R.string.pref_disable_screen_lock), false);

		if(keepAwake){
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	/*private void requestNewInterstitial() {
		AdRequest adRequest = new AdRequest.Builder()
				//.addTestDevice("964C6D356BFA085AD71A5B82995B6F14")
				.build();

		mInterstitialAd.loadAd(adRequest);
	}*/


	public void wordHighlighted(List<Integer> positions) {

		int firstPos = positions.get(0);
		int lastPos = positions.get(positions.size() - 1);
		StringBuilder forwardWord = new StringBuilder();
		StringBuilder reverseWord = new StringBuilder();
		for (Integer position : positions) {
			int row = position / cols;
			int col = position % cols;
			char c = board[row][col];
			forwardWord.append(c);
			reverseWord.insert(0, c);
		}
		
		Boolean check=true;
	
		for (Word word : solution) {
			
			int wordStart = (word.getY() * cols) + word.getX();
			
			Word found = (word.getText().equals(forwardWord.toString())) ? word : null;
			
			if (found == null) {		
				found = (word.getText().equals(reverseWord.toString())) ? word : null;
			}
		
			if (found != null) {
				boolean ok = foundWords.add(found);
				grid.goal(found);
				wordListAdapter.setWordFound(found);
				if(ok) {
					if(prefManager.getSoundState()){
						MediaPlayer mPlayer = MediaPlayer.create(this, R.raw.word_found);
						mPlayer.start();
					}	
				}
				//.break;
			}
			
			/** wrong word selected **/
			if (wordStart != firstPos && wordStart != lastPos) {
				if(prefManager.getSoundState()&&check==true){
					MediaPlayer mPlayer = MediaPlayer.create(this, R.raw.word_not_found);
					mPlayer.start();
				 	check=false;
				}	
				continue;
			}			
		}

		if (foundWords.size() == solution.size()-1) {
			puzzleFinished();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate menu resource file.
		getMenuInflater().inflate(R.menu.game, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
        if (id == R.id.action_sound) {
        	
        	Boolean sound_state = prefManager.getSoundState();
        	if(sound_state){
        		prefManager.setSoundState(false);
        		item.setIcon(R.drawable.unlock_icon);	
        	}else{
        		prefManager.setSoundState(true);
        		item.setIcon(R.drawable.lock_icon);
        	}
        	
            return true;
        }
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("NewApi")
	private void lockOrientation() {
		Display display = this.getWindowManager().getDefaultDisplay();
		int rotation = display.getRotation();
		int height;
		int width;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
			height = display.getHeight();
			width = display.getWidth();
		} else {
			Point size = new Point();
			display.getSize(size);
			height = size.y;
			width = size.x;
		}
		switch (rotation) {
			case Surface.ROTATION_90:
				if (width > height)
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				else
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
				break;
			case Surface.ROTATION_180:
				if (height > width)
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
				else
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
				break;
			case Surface.ROTATION_270:
				if (width > height)
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
				else
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				break;
			default :
				if (height > width)
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				else
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
	}

	private void puzzleFinished() {
		lockOrientation();
		grid.setEnabled(false);
		grd_word_list.setEnabled(false);
		/** if category is not finished then go to puzzle activity else back to category activity **/
		if(category.is_finished==0){
			Intent intent = new Intent(GameActivity.this, PuzzleActivity.class);
			if(prefManager.getPuzzelWord(null)==null)
				intent.putExtra("puzzel_word", puzzel_word);
			intent.putExtra("category_INFO",category);
			startActivity(intent);
		}else{
			ViewMessage("��� ����� ��� ������� �����");
		}
	}
	
	private void selectWords(){
		String count = Settings.getStringValue(this, getResources().getString(R.string.pref_key_num_words_to_select), "1000");
		category = getIntent().getExtras().getParcelable("category_INFO");
		WSDatabase wsd = new WSDatabase(this);
		wsd.open();
		wordList = wsd.getWords(category,Integer.parseInt(count));
		wsd.close();
	}

	private void showDialog(){
		dialog = new ProgressDialog(this);
		dialog.setMessage(this.getResources().getString(R.string.please_wait));
		dialog.setCancelable(false);
		dialog.show();
	}

	@Override
	public void onStop() {
		super.onStop();
		if(dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

	private void prepareBoard() {
		grid.populateBoard(board);
		List<Word> sortedWords = new ArrayList<Word>(solution);
		int rand_index = randInt(0, sortedWords.size()-1);
		Collections.sort(sortedWords);
		puzzel_word = sortedWords.get(rand_index).getText().toString();
		sortedWords.remove(rand_index);
		wordListAdapter = new WordTableAdapter(this, sortedWords);
		wordListAdapter.setWordsFound(foundWords);
		grd_word_list.setAdapter(wordListAdapter);
	}

	public static int randInt(int min, int max) {

		    Random rand = new Random(System.currentTimeMillis());
		    int randomNum = rand.nextInt((max - min) + 1) + min;
		    return randomNum;
	}
	 
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("solution", new ArrayList<Word>(solution));
		outState.putParcelableArrayList("found", new ArrayList<Word>(foundWords));
		outState.putStringArray("words", wordList);
		outState.putParcelable("category", category);

	}

	private void randomizeWords() {
		randIndices = new int[rows * cols];

		Random rand = new Random(System.currentTimeMillis());
		for (int i = 0; i < randIndices.length; i++) {
			randIndices[i] = i;
		}

		for (int i = randIndices.length - 1; i >= 1; i--) {
			int randIndex = rand.nextInt(i);
			int realIndex = randIndices[i];
			randIndices[i] = randIndices[randIndex];
			randIndices[randIndex] = realIndex;
		}

		for (int i = wordList.length - 1; i >= 1; i--) {
			int randIndex = rand.nextInt(i);
			String word = wordList[i];
			wordList[i] = wordList[randIndex];
			wordList[randIndex] = word;
		}

		for (String word : wordList) {
			addWord(word);
		}

	}

	private void addWord(String word) {
		if (word.length() > cols && word.length() > rows) {
			return;
		}

		Random rand = new Random();
		for (int i = directions.length - 1; i >= 1; i--) {
			int randIndex = rand.nextInt(i);
			Direction direction = directions[i];
			directions[i] = directions[randIndex];
			directions[randIndex] = direction;
		}

		Direction bestDirection = null;
		int bestRow = -1;
		int bestCol = -1;
		int bestScore = -1;
		for (int index : randIndices) {
			int row = index / cols;
			int col = index % cols;
			for (Direction direction : directions) {
				int score = isEmbeddable(word, direction, row, col);
				if (score > bestScore) {
					bestRow = row;
					bestCol = col;
					bestDirection = direction;
					bestScore = score;
				}
			}
		}
		if (bestScore >= 0) {
			Word result = new Word(word, bestRow, bestCol, bestDirection, this);
			embedWord(result);

		}
	}

	private void embedWord(Word word) {
		
		int curRow = word.getY();
		int curCol = word.getX();
		final String wordStr = word.getText();
		final Direction direction = word.getDirection();
		for (int i = 0; i < wordStr.length(); i++) {
			char c = wordStr.charAt(i);

			board[curRow][curCol] = c;
			lock[curRow][curCol] = true;

			if (direction.isUp()) {
				curRow -= 1;
			} else if (direction.isDown()) {
				curRow += 1;
			}

			if (direction.isLeft()) {
				curCol -= 1;
			} else if (direction.isRight()) {
				curCol += 1;
			}
		}

		solution.add(word);
	}

	private int isEmbeddable(String word, Direction direction, int row, int col) {
		if (getEmptySpace(direction, row, col) < word.length()) {
			return -1;
		}

		int score = 0;
		int curRow = row;
		int curCol = col;
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);

			if (lock[curRow][curCol] && board[curRow][curCol] != c) {
				return -1;
			} else if (lock[curRow][curCol]) {
				score++;
			}

			if (direction.isUp()) {
				curRow -= 1;
			} else if (direction.isDown()) {
				curRow += 1;
			}

			if (direction.isLeft()) {
				curCol -= 1;
			} else if (direction.isRight()) {
				curCol += 1;
			}

		}

		return score;
	}

	private int getEmptySpace(Direction direction, int row, int col) {
		switch (direction) {
			case SOUTH:
				return rows - row;
			case SOUTH_WEST:
				return Math.min(rows - row, col);
			case SOUTH_EAST:
				return Math.min(rows - row, cols - col);
			case WEST:
				return col;
			case EAST:
				return cols - col;
			case NORTH:
				return row;
			case NORTH_WEST:
				return Math.min(row, col);
			case NORTH_EAST:
				return Math.min(row, cols - col);

		}

		return 0;
	}

	private void clearBoard() {
		foundWords.clear();
		solution.clear();
		grid.clear();

		for (int i = 0; i < lock.length; i++) {
			for (int j = 0; j < lock[i].length; j++) {
				lock[i][j] = false;
			}
		}

		char c;
		if(wordList != null && wordList.length > 0)
			
			c = wordList[0].charAt(0);
		else
			c = 'A';

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = c;
			}
		}

	}

	private void ViewMessage(String msg){
		
		   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		   alertDialogBuilder.setMessage(msg);	           
		   alertDialogBuilder.setPositiveButton("����", new DialogInterface.OnClickListener() {
		   @Override
		   public void onClick(DialogInterface arg0, int arg1) {
		      //  arg0.cancel();
		        finish();
		   }
		   	});
		      
		   AlertDialog alertDialog = alertDialogBuilder.create();
		   alertDialog.show();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Runtime.getRuntime().gc();
	}
}
