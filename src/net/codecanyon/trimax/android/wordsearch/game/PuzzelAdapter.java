package net.codecanyon.trimax.android.wordsearch.game;

import java.util.ArrayList;
import java.util.List;
import net.codecanyon.trimax.android.wordsearch.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PuzzelAdapter extends BaseAdapter{

    Context context;
    List<String>data=new ArrayList<String>();
    private static LayoutInflater inflater=null;
    
    public PuzzelAdapter(Context ctx,List<String>data) {
        this.context=ctx;
        this.data=data;
        inflater = ( LayoutInflater )context.
                 getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

             rowView = inflater.inflate(R.layout.wordsearch_grid_cell, null);
             holder.tv=(TextView) rowView.findViewById(R.id.letter);

         if(data.get(position).length()>1){
        	 holder.tv.setText(data.get(position).charAt(0)+"");
        	 holder.tv.setBackgroundColor(Color.parseColor("#f08080"));
         }else if(data.get(position).length()==0){
        	 holder.tv.setVisibility(View.INVISIBLE);
         }else
        	 holder.tv.setText(data.get(position));
         
         return rowView;
    }

} 
