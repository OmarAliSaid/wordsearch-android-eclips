package net.codecanyon.trimax.android.wordsearch.game;

public class Group {
	int id,finished_category_num,is_finished,is_opened;

	public Group(int id, int finished_category_num,int is_finished,int is_opened) {
		super();
		this.id = id;
		this.finished_category_num = finished_category_num;
		this.is_finished=is_finished;
		this.is_opened=is_opened;
	}

	public int getIs_opened() {
		return is_opened;
	}

	public void setIs_opened(int is_opened) {
		this.is_opened = is_opened;
	}

	public int getIs_finished() {
		return is_finished;
	}

	public void setIs_finished(int is_finished) {
		this.is_finished = is_finished;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFinished_category_num() {
		return finished_category_num;
	}

	public void setFinished_category_num(int finished_category_num) {
		this.finished_category_num = finished_category_num;
	}
	
	
}
