package net.codecanyon.trimax.android.wordsearch.game;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nineoldandroids.animation.Animator;
import net.codecanyon.trimax.android.wordsearch.R;
import net.codecanyon.trimax.android.wordsearch.prefs.PrefManager;
import net.codecanyon.trimax.android.wordsearch.prefs.Settings;
import net.codecanyon.trimax.android.wordsearch.prefs.WSDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;


public class PuzzleActivity extends Activity {

	Button btn_help;
	TextView txt_help_num;
	TextView txt_letters;
	GridView puzzel_grid;
	PrefManager prefManager;
	private View congratView;
	Category category;
	List<String>data=new ArrayList<String>();
	List<String>data_copy=new ArrayList<String>();
	String puzzel_word; // the original puzzle word 
	String all_grid_letters; // all letters that will be viewed in the grid view
	String removed_letters=""; // wrong letters that user clicks or viewed by help
	int start_index=0;
	
	
	String[]en_letters={"q","w","e","r","t","y","u","i","o","p","l","k","j","h","g","f","d","s","a","z","x","c"
							    ,"v","b","n","m"};
	String[]ar_letters={"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"
								,"�","�","�","�","�","��","�","�","�","�","�"};
	
	private void prepareViews(){
		congratView = findViewById(R.id.checkmark);
		btn_help = (Button)findViewById(R.id.btn_help);
		txt_help_num = (TextView)findViewById(R.id.txt_help_num);
		txt_letters = (TextView)findViewById(R.id.txt_letters);
		puzzel_grid=(GridView)findViewById(R.id.grid_view_puzzel);
	}
	
	public void startNewGame(){
		prepareViews(); //initialize views
		getPuzzelWord(); //get puzzel word from sharedPref or intent if first time to open this activity
		getCategorInfo(); // get category info from sharedPref or intent if first time to open this activity 
		setTextLetters(prefManager.getSavedPuzzelSolution());
		
		ViewHelpText();
		/** after calling this function List<String>data will be ready to be populated in the grid view **/
		formDataArray();
		
		PuzzelAdapter adapter = new PuzzelAdapter(this, data);
		puzzel_grid.setAdapter(adapter);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_puzzle);
		
		prefManager = new PrefManager(this);
		startNewGame();
		
		puzzel_grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				
				TextView grid_letter = (TextView) view.findViewById(R.id.letter); //get the clicked view
				String letter = grid_letter.getText().toString(); //get text of clicked view
				String viewed_letters=txt_letters.getText().toString();
				StringBuilder mySoluation = new StringBuilder(viewed_letters);
				
				if(puzzel_word.contains(letter)){
					// find all occurrences forward of that letter 
					for (int index = puzzel_word.indexOf(letter);
						     index >= 0;
						     index = puzzel_word.indexOf(letter, index + 1))
					{
						mySoluation.setCharAt(index, puzzel_word.charAt(index));
					}
					
					setTextLetters(mySoluation.toString());
					AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
					fadeOut.setFillAfter(true);
					fadeOut.setDuration(500);
					grid_letter.startAnimation(fadeOut);
					data.set(position, "");
				}else{
					// indicator that this choice is wrong to be colored EX: ga , mn ,bk 
					// two chars indicate that this choice is wrong
					removed_letters+=data.get(position);
					data.set(position, data.get(position)+"#");
					view.setBackgroundColor(Color.parseColor("#f08080"));
				}
						
				if(isGameFinished()){
					updateCategoryInfo();
					String msg = "���� ���� �� "+puzzel_word + "\n �� ��� ��� ����";
					ViewHelpMessage(msg);
					resetPuzzle();
				}
			}
		});
		
		txt_help_num.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String a = "���� ����� �� "+puzzel_word.length();
				String b = " ���� � ����� �� "+category.getName(); 
				ViewHelpMessage(a+b);
			}
		});
		
		btn_help.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
				if(prefManager.getHelpNum()==0){
					ViewHelpMessage("����� ������ ��� ����� ������ ������ �� ���� ����� ��� �������");
				}else{
					/** look for the letter that not exsits in the puzzle word to be removed **/
					for(int i=0;i<data_copy.size();i++){
						if(puzzel_word.indexOf(data_copy.get(i)) == -1){
							
							/** check if that letter removed before **/
							if(removed_letters.contains(data_copy.get(i)))
								continue;
							else
							{
								removed_letters+=data_copy.get(i);
								data.set(i, data.get(i)+"#");
								puzzel_grid.getChildAt(i).setBackgroundColor(Color.parseColor("#f08080"));
								break;
							}	
						}
					}
					prefManager.decreaseHelpNum(); //decrease help number
					ViewHelpText(); 
				}		
			}
		});
	}

	@Override
	protected void onPause() {
		if(!isGameFinished())
			saveGameState();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.puzzle, menu);
		return true;
	}
	
	private Boolean isGameFinished(){
		return txt_letters.getText().toString().equals(puzzel_word);
	}
	
	private void saveGameState(){
		prefManager.savePuzzelWord(puzzel_word); // save the puzzle word
		prefManager.savePuzzelSolution(txt_letters.getText().toString()); // save the last solution that user reached
		
		Gson gson = new Gson();
		String category_object = gson.toJson(category); 
		prefManager.saveCategory(category_object); // save category info to be used for updating database
		
		String grid_data = gson.toJson(data);
		prefManager.saveGridDate(grid_data); // save grid data to prevent regenerate again 
	}
	
	private void resetPuzzle(){
		prefManager.resetPuzzle();
	}
	
	private void setTextLetters(String text) {
		if(text==null){
			text="";
			for(int i=0;i<puzzel_word.length();i++){
				text+="-";
			}
		}
		txt_letters.setText(text);
	}
	
	private void gameFinishAnimation(){

		AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
		fadeOut.setFillAfter(true);
		fadeOut.setDuration(500);
		fadeOut.setAnimationListener(new Animation.AnimationListener() {

			public void onAnimationStart(Animation animation) {
			}

			public void onAnimationRepeat(Animation animation) {
			}

			public void onAnimationEnd(Animation animation) {

				congratView.setVisibility(View.VISIBLE);
				if(prefManager.getSoundState()){
					MediaPlayer mPlayer = MediaPlayer.create(PuzzleActivity.this, R.raw.board_finished);
					mPlayer.start();
				}
				
				YoYo.with(Techniques.DropOut)
						.duration(700)
						.withListener(new Animator.AnimatorListener(){
							@Override
							public void onAnimationStart(Animator animation) {

							}

							@Override
							public void onAnimationEnd(Animator animation) {
								new Handler().postDelayed(new Runnable() {

									@Override
									public void run() {
										YoYo.with(Techniques.TakingOff)
										.duration(700)
										.withListener(new Animator.AnimatorListener(){
											@Override
											public void onAnimationStart(Animator animation) {

											}

											@Override
											public void onAnimationEnd(Animator animation) {
											/*	if (mInterstitialAd.isLoaded()) {

													mInterstitialAd.show();
												} else {
													startNewGame();

												}*/
											} 


											@Override
											public void onAnimationCancel(Animator animation) {

											}

											@Override
											public void onAnimationRepeat(Animator animation) {

											}
										}).playOn(congratView);
									}
								}, 2000);
							}

							@Override
							public void onAnimationCancel(Animator animation) {

							}

							@Override
							public void onAnimationRepeat(Animator animation) {

							}
						})
						.playOn(congratView);
			}
		});
		
		btn_help.startAnimation(fadeOut);
		txt_help_num.startAnimation(fadeOut);
		txt_letters.startAnimation(fadeOut);
		puzzel_grid.startAnimation(fadeOut);
		
	}
	
   private void ViewHelpMessage(String msg){
		
	   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	   alertDialogBuilder.setMessage(msg);	           
	   alertDialogBuilder.setPositiveButton("����", new DialogInterface.OnClickListener() {
	   @Override
	   public void onClick(DialogInterface arg0, int arg1) {
	        arg0.cancel();
	        if(isGameFinished())
	        	gameFinishAnimation();
	   }
	   	});
	      
	      AlertDialog alertDialog = alertDialogBuilder.create();
	      alertDialog.show();
   }
	
   private void getPuzzelWord(){
	   if(prefManager.getPuzzelWord(null)==null)
		   puzzel_word = getIntent().getExtras().getString("puzzel_word").toLowerCase();   
	   else
		   puzzel_word =  prefManager.getPuzzelWord(null); 
   }
   
   private void getCategorInfo(){
	   
	   String category_info = prefManager.getCategoryInfo();
	   if(category_info==null)
		   category = getIntent().getExtras().getParcelable("category_INFO");
	   else{
		   Gson gson = new Gson();
		   String json = prefManager.getCategoryInfo();
		   category= gson.fromJson(json, Category.class);
	   }  
   }
   
   private void ViewHelpText(){
	   txt_help_num.setText("����� �������� 5/ "+prefManager.getHelpNum());
   }
   
   private void updateCategoryInfo(){
	   
	   Runnable r = new Runnable() {
		   @Override
			public void run() {
			   WSDatabase wsd = new WSDatabase(PuzzleActivity.this);
				wsd.open();
			    category.setFinished_count(category.getFinished_count()+1);
		   	    wsd.updateCategoryInfo(category);
			    wsd.close();
		   }
	   };
	   new Thread(r).start();
	   
	}
   
   static List<String> shuffleArray(List<String> ar)
   {
     Random rnd = new Random(System.currentTimeMillis());
     for (int i = ar.size() - 1; i > 0; i--)
     {
       int index = rnd.nextInt(i + 1);
       // Simple swap
       String a = ar.get(index);
       ar.set(index, ar.get(i));
       ar.set(i, a);
     }
     return ar;
   }
   
   public static Object fromJson(String jsonString, Type type) {
	    return new Gson().fromJson(jsonString, type);
   }
   
   /**
    * the idea of this function is to randomly add 5 letters to the original puzzle_word
    * check for the saved language and append letters according to that saved language
    * the added 5 letters must be unique and already within the puzzle word 
    * all letters are added to list<String>data and shuffled randomly to be used to populate the grid view 
    */
   @SuppressWarnings("unchecked")
   private void formDataArray(){
	   
	   /** retrieve data array from shared pref **/
	   data = (ArrayList<String>)fromJson(prefManager.getGridData(),
               new TypeToken<ArrayList<String>>() {
               }.getType());
	   
	   /** check for size because if start new game no need to re generate the data array **/
	   if(data==null){
		   data=new ArrayList<String>();
		   int size=puzzel_word.length();
		   all_grid_letters = puzzel_word;
		   String [] letters;
		   if(getSelectedLang().equals("en"))
			   letters=en_letters;
		   else
			   letters=ar_letters;
		   for(int j=0;j<letters.length;j++){
			   if(!all_grid_letters.contains(letters[j])){
				   all_grid_letters+=letters[j];
			   }
			   if(all_grid_letters.length()==size+5){
				   break;
			   }
		   }
		   for(int j=0;j<all_grid_letters.length();j++){
			  data.add(String.valueOf(all_grid_letters.charAt(j)));
		   }
		   data = shuffleArray(data);
	   }
	   
	   /** copy data array **/
	   for(int i=0;i<data.size();i++){
		   data_copy.add(data.get(i));
	   }
   }
   
   private String getSelectedLang(){
	   return Settings.getStringValue(this, this.getResources().getString(R.string.pref_key_language), null);
   }
  
}
