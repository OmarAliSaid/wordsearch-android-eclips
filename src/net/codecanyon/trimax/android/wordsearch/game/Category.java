package net.codecanyon.trimax.android.wordsearch.game;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable{

	int id,group_id,is_finished,is_opened,total_size,finished_count;
	String name;

	public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            Category category=new Category();
            category.group_id=in.readInt();
            category.id=in.readInt();
            category.is_finished=in.readInt();
            category.is_opened=in.readInt();
            category.name=in.readString();
            category.total_size=in.readInt();
            category.finished_count=in.readInt();
            return category;
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(group_id);
		parcel.writeInt(id);
		parcel.writeInt(is_finished);
		parcel.writeInt(is_opened);
		parcel.writeString(name);
		parcel.writeInt(total_size);
		parcel.writeInt(finished_count);
		
		
	}
	
	public Category(int id, int group_id, String name, int is_finished,int is_opened) {
		super();
		this.id = id;
		this.group_id = group_id;
		this.name = name;
		this.is_finished = is_finished;
		this.is_opened=is_opened;
	}

	public int getIs_opened() {
		return is_opened;
	}

	public void setIs_opened(int is_opened) {
		this.is_opened = is_opened;
	}

	public Category() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIs_finished() {
		return is_finished;
	}

	public void setIs_finished(int is_finished) {
		this.is_finished = is_finished;
	}

	public int getTotal_size() {
		return total_size;
	}

	public void setTotal_size(int total_size) {
		this.total_size = total_size;
	}

	public int getFinished_count() {
		return finished_count;
	}

	public void setFinished_count(int finished_count) {
		this.finished_count = finished_count;
	}
	
}
