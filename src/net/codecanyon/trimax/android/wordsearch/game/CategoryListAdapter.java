package net.codecanyon.trimax.android.wordsearch.game;

import java.util.ArrayList;
import java.util.List;

import net.codecanyon.trimax.android.wordsearch.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryListAdapter extends BaseAdapter {
	
    List<Category>categorys=new ArrayList<Category>();
    private final LayoutInflater mInflater;
    Context ctx;

    public CategoryListAdapter(Context context,List<Category>categorys) {
        mInflater = LayoutInflater.from(context);
        this.categorys=categorys;
        this.ctx=context;
    }

    @Override
    public int getCount() {
        return categorys.size();
    }

    @Override
    public Category getItem(int i) {
        return categorys.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = mInflater.inflate(R.layout.grid_item, viewGroup, false);
            v.setTag(R.id.grid_item_picture, v.findViewById(R.id.grid_item_picture));
            v.setTag(R.id.grid_item_txt, v.findViewById(R.id.grid_item_txt));
        }

        picture = (ImageView) v.getTag(R.id.grid_item_picture);
        name = (TextView) v.getTag(R.id.grid_item_txt);
        
        if(categorys.get(position).is_opened==0)
        	picture.setImageResource(R.drawable.lock_icon );
        else
        	picture.setImageResource(R.drawable.unlock_icon );
          
        name.setText(categorys.get(position).getName());

        return v;
    }
}
