package net.codecanyon.trimax.android.wordsearch.game;

import java.util.ArrayList;
import java.util.List;
import net.codecanyon.trimax.android.wordsearch.R;
import net.codecanyon.trimax.android.wordsearch.prefs.WSDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class GroupsActivity extends Activity implements OnItemClickListener{
	
	List<Group> groups=new ArrayList<Group>();
	ListView groupsListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_groups);
		selectGroups();
		groupsListView = (ListView) findViewById(R.id.groups_list);
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		if(groups.get(position).getIs_opened()==1){
			Intent intent = new Intent(GroupsActivity.this, CategoryActivity.class);
			intent.putExtra("group_id", position+1);
			startActivity(intent);
		}
		else
		{
			Warning();
		}	
	}
		
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		selectGroups();
	}

	private void selectGroups(){
		
		Runnable r = new Runnable() {

			@Override
			public void run() {

				WSDatabase wsd = new WSDatabase(GroupsActivity.this);
				wsd.open();
				groups = wsd.getGroupsInfo();
				wsd.close();
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						GroupListAdapter customAdapter = new GroupListAdapter(GroupsActivity.this,groups);
						groupsListView.setAdapter(customAdapter);
						groupsListView.setOnItemClickListener(GroupsActivity.this);
					}
				});
			}
			
		};
		new Thread(r).start();
	}
	
	public void Warning(){
	      AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	      alertDialogBuilder.setMessage(" !! ���� .. ��� �� ����� ��������� ��������");
	      
	      alertDialogBuilder.setPositiveButton("����", new DialogInterface.OnClickListener() {
	         @Override
	         public void onClick(DialogInterface arg0, int arg1) {
	           arg0.cancel();
	         }
	      });
	      
	      AlertDialog alertDialog = alertDialogBuilder.create();
	      alertDialog.show();
    }
}
