package net.codecanyon.trimax.android.wordsearch.game;

import java.util.ArrayList;
import java.util.List;
import net.codecanyon.trimax.android.wordsearch.R;
import net.codecanyon.trimax.android.wordsearch.prefs.PrefManager;
import net.codecanyon.trimax.android.wordsearch.prefs.WSDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class CategoryActivity extends Activity implements OnItemClickListener{

	List<Category>categorys=new ArrayList<Category>();
	GridView gridView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);	
		selectCategorys();
		gridView = (GridView)findViewById(R.id.gridview);
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		selectCategorys();
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		
		PrefManager prefManager = new PrefManager(CategoryActivity.this);
		if(categorys.get(position).getIs_opened()==1)
		{
			/** check if there is saved puzzle solution from last time user played
			 * and if the category being clicked not finished before 
			 * if the two conditions achieved then move to puzzle activity 
			 */
			if(prefManager.getSavedPuzzelSolution()!=null && categorys.get(position).is_finished==0)
			{
				Intent intent = new Intent(CategoryActivity.this, PuzzleActivity.class);
				startActivity(intent);
			}
			else
			{
				categorys.get(position).setFinished_count(CountFinishedCategorys());
				categorys.get(position).setTotal_size(categorys.size());
				Intent intent = new Intent(CategoryActivity.this, GameActivity.class);
				intent.putExtra("category_INFO",categorys.get(position));
				startActivity(intent);
			}
		}else
		{
			Warning();
		}	
	}
	
	/**
	 * get categorys information from database based on selected group 
	 * the result will be list<Category>
	 */
	private void selectCategorys(){
		Runnable r = new Runnable() {

			@Override
			public void run() {

				WSDatabase wsd = new WSDatabase(CategoryActivity.this);
				wsd.open();
				categorys = wsd.getCategoyInfo(getIntent().getExtras().getInt("group_id"));
				wsd.close();
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						gridView.setAdapter(new CategoryListAdapter(CategoryActivity.this,categorys));
						gridView.setOnItemClickListener(CategoryActivity.this);
					}
				});
			}
			
		};
		new Thread(r).start();
	}
		
	/**
	 * get finished categorys count in the selected group to be used later
	 * to determine if the selected group is finished or not
	 * to determine if the next group will be opened or not
	 * 
	 * @return finished categorys count in the selected group
	 */
	public int CountFinishedCategorys(){
		int count=0;
		for(int i=0;i<categorys.size();i++){
			if(categorys.get(i).getIs_finished()==1){
				count++;
			}
		}
		return count;
		
	}
	
	/**
	 * show warning alert when user clicks on a not opened category
	 */
	public void Warning(){
	      AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
	      alertDialogBuilder.setMessage(" !! ���� .. ��� �� ����� ������� ��������");
	      
	      alertDialogBuilder.setPositiveButton("����", new DialogInterface.OnClickListener() {
	         @Override
	         public void onClick(DialogInterface arg0, int arg1) {
	        	 arg0.cancel();
	         }
	      });
	      
	      AlertDialog alertDialog = alertDialogBuilder.create();
	      alertDialog.show();
	   }

}
