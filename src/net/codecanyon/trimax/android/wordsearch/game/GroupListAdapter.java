package net.codecanyon.trimax.android.wordsearch.game;

import java.util.ArrayList;
import java.util.List;

import net.codecanyon.trimax.android.wordsearch.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class GroupListAdapter extends BaseAdapter {

	List<Group>groups=new ArrayList<Group>();
	Context ctx;
	String[] group_items_array;
	
    public GroupListAdapter(Context context,List<Group>groups) {
        this.groups=groups;
        this.ctx=context;
        group_items_array = ctx.getResources().getStringArray(R.array.group_list_values);
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return groups.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(ctx);
            v = vi.inflate(R.layout.group_item, null);
        }

        String value = group_items_array[position]+" 20/"+groups.get(position).getFinished_category_num();

        if (value != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.tv_group_item);
         
            if (tt1 != null) {
                tt1.setText(value);
            }
        }

        return v;
    }

}
