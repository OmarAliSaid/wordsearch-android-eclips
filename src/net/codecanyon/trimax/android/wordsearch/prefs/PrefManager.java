package net.codecanyon.trimax.android.wordsearch.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

	SharedPreferences.Editor editor;
	SharedPreferences setting;
	private static String MY_PREFS_NAME = "setting";
	private static String SOUND_STATE = "sound_state";
	private static String NUM_HELP = "num_help";
	private static String PUZZEL_WORD = "puzzel_word";
	private static String PUZZEL_SOLUTION = "puzzel_solution";
	private static String CATEGORY_OBJECT = "category_object";
	private static String GRID_DATA = "grid_data";
	Context ctx;
	
	public PrefManager(Context context){
		this.ctx=context;
		editor = ctx.getSharedPreferences(MY_PREFS_NAME, 0).edit();
		setting = ctx.getSharedPreferences(MY_PREFS_NAME, 0);
	}
	
	public void setSoundState(Boolean value){
		editor.putBoolean(SOUND_STATE, value);
		editor.commit();
	}
	
	public Boolean getSoundState(){
		return setting.getBoolean(SOUND_STATE, true);
	}
	
	public void decreaseHelpNum(){
		int helpNum = setting.getInt(NUM_HELP, 5);
		editor.putInt(NUM_HELP, helpNum - 1 );
		
		editor.commit();
	}
	
	public void increaseHelpNum(){
		int helpNum = setting.getInt(NUM_HELP, 5);
		editor.putInt(NUM_HELP, helpNum + 1 );
		
		editor.commit();
	}
	
	public int getHelpNum(){
		return setting.getInt(NUM_HELP, 5);
	}
	
	public String getPuzzelWord(String Default){
		return setting.getString(PUZZEL_WORD, Default);
	}
	
	public String getSavedPuzzelSolution(){
		return setting.getString(PUZZEL_SOLUTION, null);
	}
	
	public String getCategoryInfo(){
		return setting.getString(CATEGORY_OBJECT, null);
	}
	
	public String getGridData(){
		return setting.getString(GRID_DATA, null);
	}
	
	public void savePuzzelSolution(String solution){
		editor.putString(PUZZEL_SOLUTION, solution);
		editor.commit();
	}
	
	public void savePuzzelWord(String word){
		editor.putString(PUZZEL_WORD, word);
		editor.commit();
	}
	
	public void saveCategory(String categoryObject) {
		editor.putString(CATEGORY_OBJECT, categoryObject);
		editor.commit();
	}
	
	public void saveGridDate(String data) {
		editor.putString(GRID_DATA, data);
		editor.commit();
	}
	
	public void resetPuzzle(){
		editor.remove(PUZZEL_WORD);
		editor.remove(PUZZEL_SOLUTION);
		editor.remove(CATEGORY_OBJECT);
		editor.remove(GRID_DATA);
		
		editor.commit();
	}
	public void removePuzzelWord(){
		
	}
}
