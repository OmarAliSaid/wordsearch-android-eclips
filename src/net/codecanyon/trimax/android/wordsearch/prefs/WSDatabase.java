package net.codecanyon.trimax.android.wordsearch.prefs;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import net.codecanyon.trimax.android.wordsearch.game.Category;
import net.codecanyon.trimax.android.wordsearch.game.Group;
import net.codecanyon.trimax.android.wordsearch.prefs.Settings;
import net.codecanyon.trimax.android.wordsearch.R;

public class WSDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "wordsearch2.db";
    private static final int DATABASE_VERSION = 2;
    private static final String TABLE_GROUP = "catg";
    private static final String TABLE_CATEGORY = "category";
    private static final String ID = "id";
    private static final String IS_FINISHED = "Is_finished";
    private static final String IS_OPENED = "Is_opened";
    private static final String CATEGORY_NAME = "name";
    private static final String GROUP_ID = "group_id";
    private static final String FINISHED_CATEGORYS_COUNT = "finished_cat";
    private Context context;
    private String selectedLangTable;
    private SQLiteDatabase database;

    public WSDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
        this.context = context;
    }

    public void open(){
        database = getReadableDatabase();
        selectedLangTable = Settings.getStringValue(context, context.getResources().getString(R.string.pref_key_language), null);
    }

    public String[] getWords(Category category,int count){
  
         Cursor cursor = database.query(selectedLangTable+" Order BY RANDOM() LIMIT " + count,
        		 						new String[] {category.getName()}, null, null, null, null, null);
         cursor.moveToFirst();
         int len = cursor.getCount();
         String[] words = new String[len];
         int i = 0;
         while (!cursor.isAfterLast()) {
        	 words[i++] = cursor.getString(0);
             cursor.moveToNext();
         }
         cursor.close();
         return words;
     }
    
    public List<Group> getGroupsInfo(){
     
    	 String sql="SELECT * FROM "+TABLE_GROUP;
    	 Cursor cursor=database.rawQuery(sql, null);
         cursor.moveToFirst();
         List<Group>groups=new ArrayList<Group>();
         while (!cursor.isAfterLast()) {
        	 Group group = new Group(cursor.getInt(cursor.getColumnIndex(ID))
        			 				,cursor.getInt(cursor.getColumnIndex(FINISHED_CATEGORYS_COUNT))
        			 				,cursor.getInt(cursor.getColumnIndex(IS_FINISHED))
        			 				,cursor.getInt(cursor.getColumnIndex(IS_OPENED)));
        	 groups.add(group);
             cursor.moveToNext();
         }
         cursor.close();
         return groups;
     }
    
    public List<Category> getCategoyInfo(int group_id){
        
     	 Cursor cursor = database.rawQuery("select * from "+TABLE_CATEGORY+" where "+GROUP_ID+" ="+group_id+" " ,null);
     	
         cursor.moveToFirst();
         List<Category>groups=new ArrayList<Category>();
         while (!cursor.isAfterLast()) {
        	 Category category = new Category(cursor.getInt(cursor.getColumnIndex(ID)),
        			 						  cursor.getInt(cursor.getColumnIndex(GROUP_ID)),
        			 						  cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)),
        			 					   	  cursor.getInt(cursor.getColumnIndex(IS_FINISHED)),
        			 					   	  cursor.getInt(cursor.getColumnIndex(IS_OPENED)));
        	 groups.add(category);
             cursor.moveToNext();
         }
         cursor.close();
         return groups;
    }
    
    public void updateCategoryInfo(Category categroy){
    	
    	if(categroy.getFinished_count()==categroy.getTotal_size()){
    		changeFinishedState(TABLE_GROUP,categroy.getGroup_id());
    		changeOpenedState(TABLE_GROUP,categroy.getGroup_id()+1);
    	}
    	if(IsFinishedBefore(categroy.getId())==false){
    		changeFinishedState(TABLE_CATEGORY,categroy.getId());
        	changeOpenedState(TABLE_CATEGORY,categroy.getId()+1);
        	
    		database.execSQL("UPDATE "+TABLE_GROUP+" SET "+FINISHED_CATEGORYS_COUNT +" = "
    							+FINISHED_CATEGORYS_COUNT+"+1 WHERE "+ ID +" = ?",
        		    new String[] { String.valueOf(categroy.getGroup_id()) } );
    	}
    }
    
    private boolean IsFinishedBefore(int id){
    	Cursor cursor = database.rawQuery("select "+IS_FINISHED+" from "+TABLE_CATEGORY+" where "+ID+"="+id ,null);
		cursor.moveToFirst();
		int value=0;
		while (!cursor.isAfterLast()) {
			value = cursor.getInt(0);
			cursor.moveToNext();
		}
		cursor.close();
		if(value==1)
			return true;
		return false;
    }

    private void changeFinishedState(String TableName,int id){
    	database.execSQL("UPDATE "+ TableName +" SET "+IS_FINISHED+" = 1 WHERE "+ID+"=?",
    		    new Integer[]{id} );
    }
    
    private void changeOpenedState(String TableName,int id){
    	database.execSQL("UPDATE "+ TableName +" SET "+IS_OPENED+" = 1 WHERE "+ID+"=?",
    		    new Integer[]{id} );
    }
    

}
